//
//  MyView.m
//  FourSquaresProgrammaticallyWithStoryBoard
//
//  Created by Mathias Hansen on 22/01/15.
//  Copyright (c) 2015 Mathias Hedemann Hansen. All rights reserved.
//

#import "MyView.h"

@interface MyView ()
@property (nonatomic, strong) UIView *upperLeftView;
@property (nonatomic, strong) UIImageView *upperLeftInnerview;
@property (nonatomic, strong) UILabel *upperLeftLabel;
@property (nonatomic, strong) UIView *lowerLeftView;
@property (nonatomic, strong) UIImageView *lowerLeftInnerView;
@property (nonatomic, strong) UILabel *lowerLeftLabel;
@property (nonatomic, strong) UIView *lowerRightView;
@property (nonatomic, strong) UIImageView *lowerRightInnerview;
@property (nonatomic, strong) UILabel *lowerRightLabel;
@property (nonatomic, strong) UIView *upperRightView;
@property (nonatomic, strong) UIImageView *upperRightInnerview;

@end

@implementation MyView
- (id)init {
	return [self initWithFrame:CGRectZero];
}

- (id)initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];
	if (self)
    {
		// Initialization code
		[self addSubview:self.upperLeftView];
        [self.upperLeftView addSubview:self.upperLeftInnerview];
        [self.upperLeftView addSubview:self.upperLeftLabel];

        [self addSubview:self.lowerLeftView];
        [self.lowerLeftView addSubview:self.lowerLeftInnerView];
        [self.lowerLeftView addSubview:self.lowerLeftLabel];

        [self addSubview:self.lowerRightView];
        [self.lowerRightView addSubview:self.lowerRightInnerview];
        [self.lowerRightView addSubview:self.lowerRightLabel];

        [self addSubview:self.upperRightView];
        [self.upperRightView addSubview:self.upperRightInnerview];
        [self.upperRightView addSubview:self.upperRightLabel];

        [self setupConstraintsUpperLeft];
		[self setupConstraintsUpperLeftInnerView];
		[self setupConstraintsLabelUnderInnerView:self.upperLeftLabel view:self.upperLeftInnerview];

        [self setupConstraintsLowerLeft];
        [self setupConstraintsLowerLeftInnerView];
        [self setupConstraintsLabelUnderInnerView:self.lowerLeftLabel view:self.lowerLeftInnerView];

        [self setupConstraintsLowerRight];
        [self setupConstraintsLowerRightInnerView];
        [self setupConstraintsLabelUnderInnerView:self.lowerRightLabel view:self.lowerRightInnerview];

        [self setupConstraintsUpperRight];
        [self setupConstraintsUpperRightInnerView];
        [self setupConstraintsLabelUnderInnerView:self.upperRightLabel view:self.upperRightInnerview];

        //setup width upperRight
//        [self setupConstraint:self.upperRightView
//                         attribute:NSLayoutAttributeWidth
//                         relatedBy:NSLayoutRelationEqual
//                         superView:self
//                         attribute2:NSLayoutAttributeWidth
//                        multiplier:0.5f
//                          constant:0.0f];
	}
	return self;
}

- (void)updateConstraints
{
	[super updateConstraints];
}

- (UIView *)upperLeftView
{
	if (_upperLeftView == nil) {
		_upperLeftView = [[UIView alloc] init];
		[_upperLeftView setTranslatesAutoresizingMaskIntoConstraints:NO];
		_upperLeftView.backgroundColor = [UIColor redColor];
	}
	return _upperLeftView;
}
- (UIImageView *)upperLeftInnerview
{
	if (_upperLeftInnerview == nil) {
        _upperLeftInnerview = [[UIImageView alloc] init];
		[_upperLeftInnerview setTranslatesAutoresizingMaskIntoConstraints:NO];
        _upperLeftInnerview.backgroundColor = [UIColor whiteColor];
        [_upperLeftInnerview setImage:[UIImage imageNamed:@"Ray.png"]];
        _upperLeftInnerview.contentMode = UIViewContentModeScaleAspectFit;
	}
	return _upperLeftInnerview;
}
- (UILabel *)upperLeftLabel
{
	if (_upperLeftLabel == nil) {
        _upperLeftLabel = [[UILabel alloc] init];
		[_upperLeftLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
        _upperLeftLabel.text = @"Ray";
        [_upperLeftLabel sizeToFit];
    }
	return _upperLeftLabel;
}
- (UIView *)lowerLeftView
{
	if (_lowerLeftView == nil) {
		_lowerLeftView = [[UIView alloc] init];
		[_lowerLeftView setTranslatesAutoresizingMaskIntoConstraints:NO];
		_lowerLeftView.backgroundColor = [UIColor greenColor];
	}
	return _lowerLeftView;
}
- (UIImageView *)lowerLeftInnerView
{
    if (_lowerLeftInnerView == nil) {
        _lowerLeftInnerView = [[UIImageView alloc] init];
        [_lowerLeftInnerView setTranslatesAutoresizingMaskIntoConstraints:NO];
        _lowerLeftInnerView.backgroundColor = [UIColor whiteColor];
        [_lowerLeftInnerView setImage:[UIImage imageNamed:@"Dennis Ritchie.png"]];
        _lowerLeftInnerView.contentMode = UIViewContentModeScaleAspectFit;
    }
    return _lowerLeftInnerView;
}

- (UILabel *)lowerLeftLabel
{
    if (_lowerLeftLabel == nil) {
        _lowerLeftLabel = [[UILabel alloc] init];
        [_lowerLeftLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
        _lowerLeftLabel.text = @"Dennis Ritchie";
        [_lowerLeftLabel sizeToFit];
    }
    return _lowerLeftLabel;
}

- (UIView *)lowerRightView
{
	if (_lowerRightView == nil) {
		_lowerRightView  = [[UIView alloc] init];
		[_lowerRightView  setTranslatesAutoresizingMaskIntoConstraints:NO];
		_lowerRightView .backgroundColor = [UIColor blueColor];
	}
	return _lowerRightView ;
}

- (UIImageView *)lowerRightInnerview;
{
    if (_lowerRightInnerview == nil) {
        _lowerRightInnerview  = [[UIImageView alloc] init];
        [_lowerRightInnerview  setTranslatesAutoresizingMaskIntoConstraints:NO];
        _lowerRightInnerview .backgroundColor = [UIColor whiteColor];
        [_lowerRightInnerview setImage:[UIImage imageNamed:@"Brad Cox.png"]];
        _lowerRightInnerview.contentMode = UIViewContentModeScaleAspectFit;
    }
    return _lowerRightInnerview;
}

- (UILabel *)lowerRightLabel;
{
    if (_lowerRightLabel == nil) {
        _lowerRightLabel  = [[UILabel alloc] init];
        [_lowerRightLabel  setTranslatesAutoresizingMaskIntoConstraints:NO];
        _lowerRightLabel.text = @"Brad Cox";
        [_lowerRightLabel sizeToFit];
    }
    return _lowerRightLabel;
}

- (UIView *)upperRightView
{
	if (_upperRightView == nil) {
		_upperRightView  = [[UIView alloc] init];
		[_upperRightView  setTranslatesAutoresizingMaskIntoConstraints:NO];
		_upperRightView .backgroundColor = [UIColor yellowColor];
	}
	return _upperRightView ;
}

- (UIImageView *)upperRightInnerview;
{
    if(_upperRightInnerview == nil)
    {
        _upperRightInnerview = [[UIImageView alloc] init];
        [_upperRightInnerview setTranslatesAutoresizingMaskIntoConstraints:NO];
        _upperRightInnerview.backgroundColor = [UIColor whiteColor];
        [_upperRightInnerview setImage:[UIImage imageNamed:@"Matthijs.png"]];
        _upperRightInnerview.contentMode = UIViewContentModeScaleAspectFit;
    }
    return _upperRightInnerview;
}

- (UILabel *)upperRightLabel;
{
    if (_upperRightLabel == nil) {
        _upperRightLabel = [[UILabel alloc] init];
        [_upperRightLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
        _upperRightLabel.text = @"Matthijs";
        [_upperRightLabel sizeToFit];
    }
    return _upperRightLabel;
}


- (void)setupConstraintsUpperLeft
{
	// Width constraint
	[self addConstraint:[NSLayoutConstraint constraintWithItem:self.upperLeftView
													 attribute:NSLayoutAttributeWidth
													 relatedBy:NSLayoutRelationEqual
														toItem:self
													 attribute:NSLayoutAttributeWidth
													multiplier:0.5
													  constant:0]];
	// Height constraint
	[self addConstraint:[NSLayoutConstraint constraintWithItem:self.upperLeftView
													 attribute:NSLayoutAttributeHeight
													 relatedBy:NSLayoutRelationEqual
														toItem:self
													 attribute:NSLayoutAttributeHeight
													multiplier:0.5
													  constant:0]];
	// Y - left horizontally
	[self addConstraint:[NSLayoutConstraint constraintWithItem:self.upperLeftView
													 attribute:NSLayoutAttributeLeft
													 relatedBy:NSLayoutRelationEqual
														toItem:self
													 attribute:NSLayoutAttributeLeft
													multiplier:1.0
													  constant:0.0]];
	// X - top vertically
	[self addConstraint:[NSLayoutConstraint constraintWithItem:self.upperLeftView
													 attribute:NSLayoutAttributeTop
													 relatedBy:NSLayoutRelationEqual
														toItem:self
													 attribute:NSLayoutAttributeTop
													multiplier:1.0
													  constant:0.0]];
}

- (void)setupConstraintsUpperLeftInnerView;
{
    //left margin
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.upperLeftInnerview
                                                     attribute:NSLayoutAttributeLeft
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self.upperLeftView
                                                     attribute:NSLayoutAttributeLeft
                                                    multiplier:1.0f
                                                      constant:20.0f]];
    //top margin
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.upperLeftInnerview
                                                     attribute:NSLayoutAttributeTop
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self.upperLeftView
                                                     attribute:NSLayoutAttributeTop
                                                    multiplier:1.0f
                                                      constant:20.0f]];
    //right margin
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.upperLeftInnerview
                                                     attribute:NSLayoutAttributeRight
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self.upperLeftView
                                                     attribute:NSLayoutAttributeRight
                                                    multiplier:1.0f
                                                      constant:-20.0f]];
    //bottom margin
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.upperLeftInnerview
                                                     attribute:NSLayoutAttributeBottom
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self.upperLeftView
                                                     attribute:NSLayoutAttributeBottom
                                                    multiplier:1.0f
                                                      constant:-40.0f]];
    /*OLD*/
    // Width constraint
/*        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.upperLeftInnerview
                                                         attribute:NSLayoutAttributeWidth
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self.upperLeftView
                                                         attribute:NSLayoutAttributeWidth
                                                        multiplier:0.7
                                                          constant:0]];*/
    /*
// Height constraint
[self addConstraint:[NSLayoutConstraint constraintWithItem:self.upperLeftInnerview
                                                 attribute:NSLayoutAttributeHeight
                                                 relatedBy:NSLayoutRelationEqual
                                                    toItem:self.upperLeftView
                                                 attribute:NSLayoutAttributeHeight
                                                multiplier:0.7
                                                  constant:0]];
// X - left horizontally
[self addConstraint:[NSLayoutConstraint constraintWithItem:self.upperLeftInnerview
                                                 attribute:NSLayoutAttributeCenterX
                                                 relatedBy:NSLayoutRelationEqual
                                                    toItem:self.upperLeftView
                                                 attribute:NSLayoutAttributeCenterX
                                                multiplier:1.0
                                                  constant:0.0]];
// Y - top vertically
[self addConstraint:[NSLayoutConstraint constraintWithItem:self.upperLeftInnerview
                                                 attribute:NSLayoutAttributeCenterY
                                                 relatedBy:NSLayoutRelationEqual
                                                    toItem:self.upperLeftView
                                                 attribute:NSLayoutAttributeCenterY
                                                multiplier:.85
                                                  constant:0.0]];*/
}

- (void)setupConstraintsLabelUnderInnerView:(UILabel *)label view:
        (UIView *)view
{
    // Y - place label under innerview
    [self addConstraint: [NSLayoutConstraint constraintWithItem:label
                                                      attribute:NSLayoutAttributeTop
                                                      relatedBy:NSLayoutRelationEqual
                                                         toItem:view
                                                      attribute:NSLayoutAttributeBottom
                                                     multiplier:1.0f
                                                       constant:8.0f]];
    // X - horizontal center align label to innerview
    [self addConstraint:[NSLayoutConstraint constraintWithItem:label
                                                     attribute:NSLayoutAttributeCenterX
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:view
                                                     attribute:NSLayoutAttributeCenterX
                                                    multiplier:1.0f
                                                      constant:0.0f]];
}

- (void)setupConstraintsLowerLeft
{
	// Width constraint
	[self addConstraint:[NSLayoutConstraint constraintWithItem:self.lowerLeftView
													 attribute:NSLayoutAttributeWidth
													 relatedBy:NSLayoutRelationEqual
														toItem:self
													 attribute:NSLayoutAttributeWidth
													multiplier:0.5
													  constant:0]];
	// Height constraint
	[self addConstraint:[NSLayoutConstraint constraintWithItem:self.lowerLeftView
													 attribute:NSLayoutAttributeHeight
													 relatedBy:NSLayoutRelationEqual
														toItem:self
													 attribute:NSLayoutAttributeHeight
													multiplier:0.5
													  constant:0]];
	// X -left horizontally
	[self addConstraint:[NSLayoutConstraint constraintWithItem:self.lowerLeftView
													 attribute:NSLayoutAttributeLeft
													 relatedBy:NSLayoutRelationEqual
														toItem:self
													 attribute:NSLayoutAttributeLeft
													multiplier:1.0
													  constant:0.0]];
	// Y - bottom vertically
	[self addConstraint:[NSLayoutConstraint constraintWithItem:self.lowerLeftView
													 attribute:NSLayoutAttributeBottom
													 relatedBy:NSLayoutRelationEqual
														toItem:self
													 attribute:NSLayoutAttributeBottom
													multiplier:1.0
													  constant:0.0]];
}

- (void)setupConstraintsLowerLeftInnerView
{
    // left margin
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.lowerLeftInnerView
                                                     attribute:NSLayoutAttributeLeft
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self.lowerLeftView
                                                     attribute:NSLayoutAttributeLeft
                                                    multiplier:1.0f
                                                      constant:20.0f]];
    // top margin
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.lowerLeftInnerView
                                                     attribute:NSLayoutAttributeTop
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self.lowerLeftView
                                                     attribute:NSLayoutAttributeTop
                                                    multiplier:1.0f
                                                      constant:20.0f]];
    // right margin
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.lowerLeftInnerView
                                                     attribute:NSLayoutAttributeRight
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self.lowerLeftView
                                                     attribute:NSLayoutAttributeRight
                                                    multiplier:1.0
                                                      constant:-20.0f]];
    // bottom margin
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.lowerLeftInnerView
                                                     attribute:NSLayoutAttributeBottom
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self.lowerLeftView
                                                     attribute:NSLayoutAttributeBottom
                                                    multiplier:1.0f
                                                      constant:-40.0f]];
}


- (void)setupConstraintsLowerRight
{
	// Width constraint
	[self addConstraint:[NSLayoutConstraint constraintWithItem:self.lowerRightView
													 attribute:NSLayoutAttributeWidth
													 relatedBy:NSLayoutRelationEqual
														toItem:self
													 attribute:NSLayoutAttributeWidth
													multiplier:0.5
													  constant:0]];
	// Height constraint
	[self addConstraint:[NSLayoutConstraint constraintWithItem:self.lowerRightView
													 attribute:NSLayoutAttributeHeight
													 relatedBy:NSLayoutRelationEqual
														toItem:self
													 attribute:NSLayoutAttributeHeight
													multiplier:0.5
													  constant:0]];
	// X - Right horizontally
	[self addConstraint:[NSLayoutConstraint constraintWithItem:self.lowerRightView
													 attribute:NSLayoutAttributeRight
													 relatedBy:NSLayoutRelationEqual
														toItem:self
													 attribute:NSLayoutAttributeRight
													multiplier:1.0
													  constant:0.0]];
	//Y - bottom vertically
	[self addConstraint:[NSLayoutConstraint constraintWithItem:self.lowerRightView
													 attribute:NSLayoutAttributeBottom
													 relatedBy:NSLayoutRelationEqual
														toItem:self
													 attribute:NSLayoutAttributeBottom
													multiplier:1.0
													  constant:0.0]];
}
- (void)setupConstraintsLowerRightInnerView;
{
    //left margin
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.lowerRightInnerview
                                                     attribute:NSLayoutAttributeLeft
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self.lowerRightView
                                                     attribute:NSLayoutAttributeLeft
                                                    multiplier:1.0f
                                                      constant:20.0f]];

    //Top margin
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.lowerRightInnerview
                                                     attribute:NSLayoutAttributeTop
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self.lowerRightView
                                                     attribute:NSLayoutAttributeTop
                                                    multiplier:1.0f
                                                      constant:20.0f]];
    // right margin
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.lowerRightInnerview
                                                     attribute:NSLayoutAttributeRight
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self.lowerRightView
                                                     attribute:NSLayoutAttributeRight
                                                    multiplier:1.0f
                                                      constant:-20.0f]];

    // bottom margin
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.lowerRightInnerview
                                                     attribute:NSLayoutAttributeBottom
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self.lowerRightView
                                                     attribute:NSLayoutAttributeBottom
                                                    multiplier:1.0f
                                                      constant:-40.0f]];

}

- (void)setupConstraintsUpperRight
{
	// Width constraint
	[self addConstraint:[NSLayoutConstraint constraintWithItem:self.upperRightView
													 attribute:NSLayoutAttributeWidth
													 relatedBy:NSLayoutRelationEqual
														toItem:self
													 attribute:NSLayoutAttributeWidth
													multiplier:0.5
													  constant:0]];
	
	// Height constraint
	[self addConstraint:[NSLayoutConstraint constraintWithItem:self.upperRightView
													 attribute:NSLayoutAttributeHeight
													 relatedBy:NSLayoutRelationEqual
														toItem:self
													 attribute:NSLayoutAttributeHeight
													multiplier:0.5
													  constant:0]];
	
	// X - Right horizontally
	[self addConstraint:[NSLayoutConstraint constraintWithItem:self.upperRightView
													 attribute:NSLayoutAttributeRight
													 relatedBy:NSLayoutRelationEqual
														toItem:self
													 attribute:NSLayoutAttributeRight
													multiplier:1.0
													  constant:0.0]];
	
	// Y - Top vertically
	[self addConstraint:[NSLayoutConstraint constraintWithItem:self.upperRightView
													 attribute:NSLayoutAttributeTop
													 relatedBy:NSLayoutRelationEqual
														toItem:self
													 attribute:NSLayoutAttributeTop
													multiplier:1.0
													  constant:0.0]];
}

- (void)setupConstraintsUpperRightInnerView;
{
    // left margin
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.upperRightInnerview
                                                     attribute:NSLayoutAttributeLeft
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self.upperRightView
                                                     attribute:NSLayoutAttributeLeft
                                                    multiplier:1.0f
                                                      constant:20.0f]];
    // top margin
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.upperRightInnerview
                                                     attribute:NSLayoutAttributeTop
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self.upperRightView
                                                     attribute:NSLayoutAttributeTop
                                                    multiplier:1.0f
                                                      constant:20.0f]];
    // right margin
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.upperRightInnerview
                                                     attribute:NSLayoutAttributeRight
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self.upperRightView
                                                     attribute:NSLayoutAttributeRight
                                                    multiplier:1.0
                                                      constant:-20.0f]];
    // bottom margin
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.upperRightInnerview
                                                     attribute:NSLayoutAttributeBottom
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self.upperRightView
                                                     attribute:NSLayoutAttributeBottom
                                                    multiplier:1.0
                                                      constant:-40.0f]];
}

- (void)setupConstraint:(UIView *)theView
              attribute:(NSLayoutAttribute)attribute
              relatedBy:(NSLayoutRelation)relatedBy
              superView:(UIView *)superView
             attribute2:(NSLayoutAttribute)attribute2
             multiplier:(float)multiplier
               constant:(float)constant;
{
	[self addConstraint:[NSLayoutConstraint constraintWithItem:theView
													 attribute:attribute
													 relatedBy:relatedBy
														toItem:superView
													 attribute:attribute2
													multiplier:multiplier
													  constant:constant]];
}




/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end

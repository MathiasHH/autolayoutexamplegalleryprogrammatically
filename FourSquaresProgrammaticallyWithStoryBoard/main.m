//
//  main.m
//  FourSquaresProgrammaticallyWithStoryBoard
//
//  Created by Mathias Hansen on 22/01/15.
//  Copyright (c) 2015 Mathias Hedemann Hansen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
	}
}

//
//  AppDelegate.h
//  FourSquaresProgrammaticallyWithStoryBoard
//
//  Created by Mathias Hansen on 22/01/15.
//  Copyright (c) 2015 Mathias Hedemann Hansen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

